-- Вывести в таблицу с группами группы "К3-{sn}{gn}Б", где sn - номер семестра (от 1 до 8), а gn  - номер группы (от 0 до 6)

use `students` ;

drop procedure if exists while_loop;
DELIMITER //

create procedure while_loop()
begin
      declare result varchar(10) default '';
      declare i int default 5;
      declare semester INT default 1;
	  declare group_number INT default 5;
      while semester <= 8 DO
         set result = concat('К4-', semester, group_number, 'Б');
         insert into students_group (id, name) values (i, result);
         if (group_number in (6)) then 
			 set group_number = 0; 
			 set semester = semester + 1;
         else 
			 set group_number = group_number + 1;
         end if;
         set i = i + 1;
      end while;
end //
   
DELIMITER ;


begin;
call while_loop();
select * from students_group;
rollback;

