SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';


-- -----------------------------------------------------
-- Schema students
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `students` ;
CREATE SCHEMA IF NOT EXISTS `students` DEFAULT CHARACTER SET utf8 ;
USE `students` ;


-- -----------------------------------------------------
-- Table `students`.`discipline`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `students`.`discipline` (
  `id` INT NOT NULL,
  `name` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `students`.`students_group`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `students`.`students_group` (
  `id` INT NOT NULL,
  `name` VARCHAR(10) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `students`.`student`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `students`.`student` (
  `id` INT NOT NULL,
  `first_name` VARCHAR(45) NOT NULL,
  `last_name` VARCHAR(45) NOT NULL,
  `middle_name` VARCHAR(45) NULL,
  `gradebook_number` VARCHAR(45) NOT NULL,
  `group_id` INT NOT NULL,
  `number_in_group` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_student_group_idx` (`group_id` ASC) VISIBLE,
  CONSTRAINT `fk_student_group`
    FOREIGN KEY (`group_id`)
    REFERENCES `students`.`students_group` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `students`.`assessment`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `students`.`assessment` (
  `discipline_id` INT NOT NULL,
  `student_id` INT NOT NULL,
  `assessment` VARCHAR(15) NOT NULL,
  `id` INT NOT NULL AUTO_INCREMENT,
  `assessment_type` VARCHAR(15) NULL,
  INDEX `fk_discipline_has_student_student1_idx` (`student_id` ASC) VISIBLE,
  INDEX `fk_discipline_has_student_discipline1_idx` (`discipline_id` ASC) VISIBLE,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_discipline_has_student_discipline1`
    FOREIGN KEY (`discipline_id`)
    REFERENCES `students`.`discipline` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_discipline_has_student_student1`
    FOREIGN KEY (`student_id`)
    REFERENCES `students`.`student` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
