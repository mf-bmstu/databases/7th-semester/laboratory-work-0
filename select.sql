use `students` ;

-- Тестовый запрос по всей информации из бд
select student.number_in_group, student.first_name, student.last_name, student.middle_name, student.gradebook_number, students_group.name, discipline.name, assessment.assessment, assessment.assessment_type
from student
inner join students_group on student.group_id = students_group.id
inner join assessment on student.id = assessment.student_id
inner join discipline on assessment.discipline_id = discipline.id;

-- Найти сутдентов которых необоходимо отчислить (по всем дисциплинам только "Я")
select student.number_in_group, student.first_name, student.last_name, student.middle_name, student.gradebook_number, students_group.name as group_name, count(assessment.assessment) as amount_of_not_passed
from student
inner join students_group on student.group_id = students_group.id
inner join assessment on student.id = assessment.student_id
where assessment.assessment = 'Я'
group by student.id 
having  count(assessment.assessment) = (select count(*) from discipline)
order by count(assessment.assessment) desc;

-- Найти сутдентов которых имеющих оценку "Отл" по всем дисциплинамa
select student.number_in_group, student.first_name, student.last_name, student.middle_name, student.gradebook_number, students_group.name as group_name, count(assessment.assessment) as amount_of_not_passed
from student
inner join students_group on student.group_id = students_group.id
inner join assessment on student.id = assessment.student_id
inner join discipline on assessment.discipline_id = discipline.id
where assessment.assessment = 'Отл'
group by student.id 
having  count(assessment.assessment) = (select count(*) from discipline)
order by count(assessment.assessment) desc;

-- Вывести самые многочисленные по колличеству студентов группы
select students_group.name
from student
inner join students_group on student.group_id = students_group.id
group by students_group.id
having count(*) = (
	select max(t1.students_count) 
    from (
		select count(*) as students_count
		from student
		inner join students_group on student.group_id = students_group.id
		group by students_group.id
	) as t1
);

-- Вывести общее количество оценок "Я" для всех групп
select students_group.name as group_name, count(assessment.assessment) as amount_of_not_passed
from student
inner join students_group on student.group_id = students_group.id
inner join assessment on student.id = assessment.student_id
where assessment.assessment = 'Я'
group by students_group.id;

-- Вывести отношение оценок "Я" к общему числу оценок для каждой группы
select t1.name, (t1.amount_of_not_passed / t2.all_amount) as koef
from (
	select students_group.name as name, students_group.id as id, count(assessment.assessment) as amount_of_not_passed
	from student
	inner join students_group on student.group_id = students_group.id
	inner join assessment on student.id = assessment.student_id
	where assessment.assessment = 'Я'
	group by students_group.id
) as t1
inner join (
	select students_group.id as id, count(assessment.assessment) as all_amount
	from student
	inner join students_group on student.group_id = students_group.id
	inner join assessment on student.id = assessment.student_id
	group by students_group.id
) as t2 on t1.id = t2.id;

-- Вывести количество оценок "Отл" для каждой дисциплины
select discipline.name, count(assessment.assessment)
from discipline
inner join assessment on discipline.id = assessment.discipline_id
where assessment.assessment = "Отл"
group by discipline.id;

-- Вывести всех студентов у которых нет ни одной оценки "Я"
select student.number_in_group, student.first_name, student.last_name, student.middle_name, student.gradebook_number, students_group.name as group_name
from student
inner join students_group on student.group_id = students_group.id
inner join assessment on student.id = assessment.student_id
where assessment.assessment != 'Я'
group by student.id 

